### Komi

In normal games, black always plays the first move.  Thus, black has a slight
advantage against white.  To compensate this unfairness, white is sometimes
given bonus points, called Komi, when counting territories in the end.

This bonus is usually 6.5 points. That means the value of the first move is
considered to be about 6 and a half points.  The additional half point is
introduced to prevent draw.

For example, if black has 7 points more territories than white, black wins by
0.5 point.  If black has 6 more points than white, white wins by 0.5 point.

In the old days of Go, there was no Komi system. As more people become aware of
the black's advantage, Komi was introduced.

The amount of Komi is changing over time. When it was introduced in Japanese
Professional games, it was 4.5 points.  However, black still had better chance
to win, so Komi was increased to 5.5 points in 1974. In 2002, the Japanese Go
Association again increased the Komi value to 6.5.

With this Komi system, Go has almost no drawn games and has become more
exciting and fairer.

---

### The Handicap Game

When the strength of the two players are different, the weaker side places some
number of stones before the game begins for a handicap.  In handicap games, the
weaker side always plays black and stronger side plays white.

Black places handicap stones on the specified points and then white plays the
first move.

The number of handicap stones reflects the difference of the two players.

If the weaker player is 5 Kyu and the stronger player is 2 Kyu, 3 handicap
would be used.

With the handicap stones, black has advantage over every aspect of the game -
attacking, defending, surrounding territories, etc.

It is said that one handicap stone is equivalent to 10 points of territories.

Thus, if you played an even game with me and lost by 50 points, a 5 handicap
game would be appropriate for us to give us both an even chance to win.

The position of handicap stones is conventionally specified as below.

---

9 Handicap

Board::komi0

---

6 Handicap

Board::komi1

---

5 Handicap

Board::komi2

---

4 Handicap

Board::komi3

---

In every case, handicap stones are placed on the 4-4 points (star points) which
are marked with small black circle. However, you can have more than 9 handicap
stones by placing stones elsewhere.
With the handicap game, everyone can enjoy playing Go with the same chance to win even no matter how different the players' strength are, without altering the rule.
