In this lesson we'll learn about ladders.

Capture the white stone.  Keep making successive ataris as white tries to
escape.

Board::ladder-1

Did you get it?

We call the shape a **ladder** because the resulting shape ends up looking like
a series of steps.  Honestly, it looks more like a **staircase**, but the Go
community has already decided on **ladder**.  Oh, well.

---

Here's another ladder.

Unfortunately, it's your turn to escape.  However, this time, escape is
possible.

Good luck!

Board::ladder-2

Could you escape from white's persistent attack?

If there are friendly stones in the path of the ladder, you can escape from the
attack because you can extend yourself more quickly.

## Go proverbs

[Go proverbs][go-proverbs] are woven through the fabric of the community to
summarize wisdom into easy-to-remember phrases.

You will often hear such wisdom dispensed during game commentary or lessons.

Here is the proverb for this lesson.

> Don't play Go if you don't know the ladder.

The main idea behind this proverb is that learning the ladder is not very
difficult compared to learning Go as a whole.  If one should decide to play Go,
one should invest the time to learn the ladder.

[go-proverbs]: http://senseis.xmp.net/?GoProverbs
