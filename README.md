# Way to Go

This project is a modern remake of Hiroki Mori's [Interactive Way to Go][iwtg].

[iwtg]: https://web.archive.org/web/20210831172631/http://playgo.to/iwtg/en/
